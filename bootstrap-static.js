(function() {
	try {
		$.fn.bootstrapStatic = function(opts) {
			var me = this;
			
			var mqtest = '<style id="bootstrap-static-mq-test">@media all and (min-width:1px){.mqtest{position:absolute}}</style>';
			
			$("head").append(mqtest);
			
			mqtest = $('<div class="mqtest"></div>');
			
			$("html").append(mqtest);

			if(window.getComputedStyle && window.getComputedStyle(mqtest.get(0)).position == "absolute") {
				var mediaQueriesSupported = true;
			}
			else {
				var mediaQueriesSupported = false;
			}

			$(mqtest).remove();

			$("#bootstrap-static-mq-test").remove();

			if(!opts) {
				var opts = {};
			}
		
			if(opts.debug) {
				var debug = opts.debug;
			}
			else {
				var debug = false;
			}

			var styles = "<style>.bootstrap-static .display-block-important{display:block !important;} .display-none-important {display:none !important;}</style>";

			var size = opts.size || "lg";
		
			var defaultMinWidths = {
				"lg" : 1200,
				"md" : 992, 
				"sm" : 768,
				"xs" : -1
				};

			var minWidth = opts.minWidth || defaultMinWidths[size];

			var sizes = {};

			sizes["lg"] = function() {
				var styleCss = "<style>";
				styleCss += ".container { width: 1170px; margin: 0 auto; padding-left: 15px; padding-right: 15px; }";
				styleCss += ".col-lg-1, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-10, .col-lg-11, .col-lg-12 { float:left; }";
				styleCss += ".col-lg-1 { display:block; width: 8.33333333%; }";
				styleCss += ".col-lg-2 { display:block; width: 16.66666667%; }";
				styleCss += ".col-lg-3 { display:block; width: 25%; }";
				styleCss += ".col-lg-4 { display:block; width: 33.33333333%; }";
				styleCss += ".col-lg-5 { display:block; width: 41.66666667%; }";
				styleCss += ".col-lg-6 { display:block; width: 50%; }";
				styleCss += ".col-lg-7 { display:block; width: 58.33333333%; }";
				styleCss += ".col-lg-8 { display:block; width: 66.66666667%; }";
				styleCss += ".col-lg-9 { display:block; width: 75%; }";
				styleCss += ".col-lg-10 { display:block; width: 83.33333333%; }";
				styleCss += ".col-lg-11 { display:block; width: 91.66666667%; }";
				styleCss += ".col-lg-12 { display:block; width: 100%; }";
				
				styleCss += ".col-lg-pull-12 { right: 100%; }";
				styleCss += ".col-lg-pull-11 { right: 91.66666667%; }";
				styleCss += ".col-lg-pull-10 { right: 83.33333333%; }";
				styleCss += ".col-lg-pull-9 { right: 75%; }";
				styleCss += ".col-lg-pull-8 { right: 66.66666667%; }";
				styleCss += ".col-lg-pull-7 { right: 58.33333333%; }";
				styleCss += ".col-lg-pull-6 { right: 50%; }";
				styleCss += ".col-lg-pull-5 { right: 41.66666667%; }";
				styleCss += ".col-lg-pull-4 { right: 33.33333333%; }";
				styleCss += ".col-lg-pull-3 { right: 25%; }";
				styleCss += ".col-lg-pull-2 { right: 16.66666667%; }";
				styleCss += ".col-lg-pull-1 { right: 8.33333333%; }";
				styleCss += ".col-lg-pull-0 { right: auto; }";
				styleCss += ".col-lg-push-12 { left: 100%; }";
				
				styleCss += ".col-lg-push-11 { left: 91.66666667%; }";
				styleCss += ".col-lg-push-10 { left: 83.33333333%; }";
				styleCss += ".col-lg-push-9 { left: 75%; }";
				styleCss += ".col-lg-push-8 { left: 66.66666667%; }";
				styleCss += ".col-lg-push-7 { left: 58.33333333%; }";
				styleCss += ".col-lg-push-6 { left: 50%; }";
				styleCss += ".col-lg-push-5 { left: 41.66666667%; }";
				styleCss += ".col-lg-push-4 { left: 33.33333333%; }";
				styleCss += ".col-lg-push-3 { left: 25%; }";
				styleCss += ".col-lg-push-2 { left: 16.66666667%; }";
				styleCss += ".col-lg-push-1 { left: 8.33333333%; }";
				styleCss += ".col-lg-push-0 { left: auto; }";
				
				styleCss += ".col-lg-offset-0 { margin-left: 0; }";
				styleCss += ".col-lg-offset-1 { margin-left: 8.33333333%; }";
				styleCss += ".col-lg-offset-2 { margin-left: 16.66666667%; }";
				styleCss += ".col-lg-offset-3 { margin-left: 25%; }";
				styleCss += ".col-lg-offset-4 { margin-left: 33.33333333%; }";
				styleCss += ".col-lg-offset-5 { margin-left: 41.66666667%; }";
				styleCss += ".col-lg-offset-6 { margin-left: 50%; }";
				styleCss += ".col-lg-offset-7 { margin-left: 58.33333333%; }";
				styleCss += ".col-lg-offset-8 { margin-left: 66.66666667%; }";
				styleCss += ".col-lg-offset-9 { margin-left: 75%; }";
				styleCss += ".col-lg-offset-10 { margin-left: 83.33333333%; }";
				styleCss += ".col-lg-offset-11 { margin-left: 91.66666667%; }";
				styleCss += ".col-lg-offset-12 { margin-left: 100%; }";
				
				styleCss += "</style>";
				$("body").append(styleCss);
				$(".visible-lg-block").addClass("display-block-important");
				$(".visible-lg-inline-block").addClass("display-inline-block-important");			
			};

			sizes["md"] = function() {
				var removeClasses = ".col-lg-1 col-lg-2 col-lg-3 col-lg-4 col-lg-5 col-lg-6 col-lg-7 col-lg-8 col-lg-9 col-lg-10 col-lg-11 col-lg-12";
				$("*").removeClass(removeClasses);
				
				var styleCss = "<style>";
				
				styleCss += ".container { width: 970px; margin: 0 auto; padding-left: 15px; padding-right: 15px; }";
				styleCss += ".col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12 { display:block !important; float:left; }";
				
				styleCss += ".col-md-1 { display:block; width: 8.33333333%; }";
				styleCss += ".col-md-2 { display:block; width: 16.66666667%; }";
				styleCss += ".col-md-3 { display:block; width: 25%; }";
				styleCss += ".col-md-4 { display:block; width: 33.33333333%; }";
				styleCss += ".col-md-5 { display:block; width: 41.66666667%; }";
				styleCss += ".col-md-6 { display:block; width: 50%; }";
				styleCss += ".col-md-7 { display:block; width: 58.33333333%; }";
				styleCss += ".col-md-8 { display:block; width: 66.66666667%; }";
				styleCss += ".col-md-9 { display:block; width: 75%; }";
				styleCss += ".col-md-10 { display:block; width: 83.33333333%; }";
				styleCss += ".col-md-11 { display:block; width: 91.66666667%; }";
				styleCss += ".col-md-12 { display:block; width: 100%; }";

				styleCss += ".col-md-pull-12 { right: 100%; }";
				styleCss += ".col-md-pull-11 { right: 91.66666667%; }";
				styleCss += ".col-md-pull-10 { right: 83.33333333%; }";
				styleCss += ".col-md-pull-9 { right: 75%; }";
				styleCss += ".col-md-pull-8 { right: 66.66666667%; }";
				styleCss += ".col-md-pull-7 { right: 58.33333333%; }";
				styleCss += ".col-md-pull-6 { right: 50%; }";
				styleCss += ".col-md-pull-5 { right: 41.66666667%; }";
				styleCss += ".col-md-pull-4 { right: 33.33333333%; }";
				styleCss += ".col-md-pull-3 { right: 25%; }";
				styleCss += ".col-md-pull-2 { right: 16.66666667%; }";
				styleCss += ".col-md-pull-1 { right: 8.33333333%; }";
				styleCss += ".col-md-pull-0 { right: auto; }";
				styleCss += ".col-md-push-12 { left: 100%; }";

				styleCss += ".col-md-push-11 { left: 91.66666667%; }";
				styleCss += ".col-md-push-10 { left: 83.33333333%; }";
				styleCss += ".col-md-push-9 { left: 75%; }";
				styleCss += ".col-md-push-8 { left: 66.66666667%; }";
				styleCss += ".col-md-push-7 { left: 58.33333333%; }";
				styleCss += ".col-md-push-6 { left: 50%; }";
				styleCss += ".col-md-push-5 { left: 41.66666667%; }";
				styleCss += ".col-md-push-4 { left: 33.33333333%; }";
				styleCss += ".col-md-push-3 { left: 25%; }";
				styleCss += ".col-md-push-2 { left: 16.66666667%; }";
				styleCss += ".col-md-push-1 { left: 8.33333333%; }";
				styleCss += ".col-md-push-0 { left: auto; }";

				styleCss += ".col-md-offset-0 { margin-left: 0; }";
				styleCss += ".col-md-offset-1 { margin-left: 8.33333333%; }";
				styleCss += ".col-md-offset-2 { margin-left: 16.66666667%; }";
				styleCss += ".col-md-offset-3 { margin-left: 25%; }";
				styleCss += ".col-md-offset-4 { margin-left: 33.33333333%; }";
				styleCss += ".col-md-offset-5 { margin-left: 41.66666667%; }";
				styleCss += ".col-md-offset-6 { margin-left: 50%; }";
				styleCss += ".col-md-offset-7 { margin-left: 58.33333333%; }";
				styleCss += ".col-md-offset-8 { margin-left: 66.66666667%; }";
				styleCss += ".col-md-offset-9 { margin-left: 75%; }";
				styleCss += ".col-md-offset-10 { margin-left: 83.33333333%; }";
				styleCss += ".col-md-offset-11 { margin-left: 91.66666667%; }";
				styleCss += ".col-md-offset-12 { margin-left: 100%; }";

				styleCss += "</style>";
				$("body").append(styleCss);
				$(".visible-lg-block, .visible-lg-inline-block").addClass("display-none-important");
				$(".visible-md-block").addClass("display-block-important");
				$(".visible-md-inline-block").addClass("display-inline-block-important");
			}

			sizes["sm"] = function() {
				//todo 
			}

			sizes["xs"] = function() {
				//todo
			}

			var updateDom = function() {
				if(debug == true) {
					console.info("Running update  with size: " + size);
				}
				if(sizes[size]) {
					sizes[size]();
				}
				else {
					throw "BootstrapStatic error: '" + size + "' is not a valid size in the bootstrap grid. use lg || md || sm || xs";
				}
			}


			var updateMinWidth = function() {
				if($(window).width() < minWidth) {
					if(debug == true) {
						console.info("Window is below minwidth. Set to size: " + minWidth);
					}
					$("body").width(minWidth);
				}
				else {
					if(debug == true) {
						console.info("Window is >= minwidth. Set to size: 100%");
					}
					$("body").css("width", "100%");
				}
			}

			var getNodeTypeOrId = function() {
				var n = $(me).prop("nodeName").replace("#", "");
				if(n == "document" || n == "body" || n == "html") {
					return n;
				}
				else {
					return "#" + $(me).attr("id");
				}
			}

			me.setDefaultImageSizes = function() {
				if(mediaQueriesSupported == false) {
					var images = $("img");
					for(var i = 0; i < images.length; i++) {
						var image = images[i];
						$(image).removeAttr("width").removeAttr("height");
						$(image).css({
							"width": $(image).outerWidth() + "px"
						});
					}
				}
			}

			if(mediaQueriesSupported == false) {
				$(document).ready(function() {
					$("html").addClass("bootstrap-static");

					$("body").append(styles);
					
					updateDom();
					
					$(window).resize(updateMinWidth);
						
					window.bssImageRefresh = me.setDefaultImageSizes;
					
					$("body").attr("onpropertychange", "window.bssImageRefresh();");
				});

				if(opts.updateImageWidths && opts.updateImageWidths == false) {

				}
				else {
					$(window).load(function() {
						me.setDefaultImageSizes();
					});
				}
				
				updateMinWidth();
			}
		}
	}
	catch(err) {
		throw "Could not set up BootstrapStatic JQuery Plugin. ERROR: " + err;
	}
})();


