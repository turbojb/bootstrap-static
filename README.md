BoostrapStatic is a jquery plugin. It makes bootstrap sites statically sized in old browsers that do not support media queries. It also statically sizes images based on the images natural size. I created it very quickly. It is not yet complete but I have used it in many production apps.

Usage:

```
#!javascript
$(document).bootstrapStatic({debug: false, size: "lg"});

```

options

* **debug:** true | false - shows debug info
* **size:** 'lg' || 'md' || 'sm' || 'xs' - bootstrap grid size to show statically (I have not implemented 'sm' or 'xs' yet)

good times!